//
//  Extensions.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 28/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

extension String {
    func sanitizedString() -> String {
        let chars = Set<Character>(" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".characters)
        return String(self.characters.filter( { chars.contains($0) } ))
    }
}

extension RangeReplaceableCollectionType where Generator.Element : Equatable {
    mutating func removeObject(object : Generator.Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
}

extension Array {
    func sample() -> Element {
        let randomIndex = Int(rand()) % count
        return self[randomIndex]
    }
}
