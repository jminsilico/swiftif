//
//  DreamwebChapter.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 29/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

class DreamwebChapter: BaseChapter {
    
    init() {
        let inventory = Location(name: "", description: "")
        super.init(inventory: inventory)
        
        self.addWord(.Verb, keyword: "use", alternatives: ["wear"])
        self.addWord(.Noun, keyword: "watch")
        self.addWord(.Noun, keyword: "jeans", alternatives: ["trousers"])
        self.addWord(.Noun, keyword: "shirt")
        self.addWord(.Noun, keyword: "trainers", alternatives: ["shoes"])
        self.addWord(.Noun, keyword: "ring")
        self.addWord(.Noun, keyword: "gum")
        self.addWord(.Noun, keyword: "shades", alternatives: ["glasses", "sunglasses"])
        
        let edensBedroom = Location(name: "Eden's Bedroom", description: "You are in Eden's bedroom.  Eden is lying in bed.")
        self.currentLocation = edensBedroom
        
        let watch = Item(keyword: "watch", shortDescription: "A watch", longDescription: "The watch that Eden bought you for your last birthday has a gunmetal grey face and a worn leather strap.  Several small buttons on the sides control the watch.", location: inventory)
        let jeans = Item(keyword: "jeans", shortDescription: "A pair of jeans", longDescription: "Probably the only pair of jeans you have.  They could virtually walk around by themselves.  They are torn, dirty and very comfortable.  One pocket at the back has almost come loose.", location: inventory)
        let shirt = Item(keyword: "shirt", shortDescription: "A shirt", longDescription: "A long sleeved purple shirt with buttons that reach half way down the front. The materail is rough to the touch on the outside, but smooth on the inside.  A button is missing.", location: inventory)
        let trainers = Item(keyword: "trainers", shortDescription: "A pair of trainers", longDescription: "A pair of expensive training shoes with two brillaint green flashes down each side.  The shoes have a  Velcro fastener across the front.", location: inventory)
        let ring = Item(keyword: "ring", shortDescription: "A ring", longDescription: "A gold ring that you wear on your right hand.  The ring is quite old and shows its age.  Around the edge of the ring your name is engraved in tiny letters.", location: inventory)
        let gum = Item(keyword: "gum", shortDescription: "Some gum", longDescription: "A packet of mint flavoured chewing gum with three green stripes across the packaging.  The packet is made from tin foil.", location: inventory)
        let shades = Item(keyword: "shades", shortDescription: "A pair of shades", longDescription: "A battered pair of plastic  black sunglasses that you've had for ages.  The smog outside rarely lets through enough light to make them necessary, but you wear them anyway most of the time.", location: inventory)
        
        var wornItems = [watch, jeans, trainers, ring]
        
        self.addRule(["use shades"], conditions: [], actions: [
            { (command) in
                if !wornItems.contains(shades) {
                    wornItems.append(shades)
                    self.delegate?.chapter(self, shouldWrite: "You feel much better.")
                }
            }
        ])
        
        self.addRule(["use gum"], conditions: [], actions: [
            { (command) in
                self.delegate?.chapter(self, shouldWrite: "Unwrapping the silver foil you take out a stick of gum and bend it into your mouth.")
            }
        ])
        
        //        "A simple piece of chewing gum wrapped in foil."
        //        "Ahhh minty."
        //        ""
        
        let eden = Item(keyword: "eden", shortDescription: "Your girlfriend, Eden", longDescription: "Your girlfriend Eden lies in bed with her eyes half closed and the covers pulled tightly around her.  Her dark hair spills out over the pillow.  She has a slight frown on her face", location: edensBedroom)
        

        
        
        
        self.items = [watch, jeans, shirt, trainers, ring, gum, shades]
    }
    
}
