//
//  Chapter1.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 26/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

class BaseChapter: Chapter {
    
    weak var delegate: ChapterDelegate?
    
    var currentLocation: Location!
    var inventory: Location
    
    var items = [Item]()
    
    var directions: [Word] {
        return self._words.filter( { $0.type == WordType.Direction } )
    }
    var verbs: [Word] {
        return self._words.filter( { $0.type == WordType.Verb } )
    }
    var nouns: [Word] {
        return self._words.filter( { $0.type == WordType.Noun } )
    }
    private var _words = [Word]()
    var words: [Word] {
        return self._words
    }
    
    var rules = [Rule]()
    
    init(inventory: Location) {
        self.inventory = inventory
        self.addWord(.Verb, keyword: "inventory", alternatives: ["i", "inv"])
        self.addRule(["inventory"], conditions: [], actions: [{ (command) in
            self.delegate?.chapter(self, shouldWrite: "You have:")
            let inventoryItems = self.items.filter( { $0.location == self.inventory } )
            if inventoryItems.count == 0 {
                let randomItem = ["Your health.", "All the time in the world.", "No tea.", "A splitting headache"].sample()
                self.delegate?.chapter(self, shouldWrite: randomItem)
            } else {
                self.delegate?.chapter(self, shouldWrite: inventoryItems.map( { $0.shortDescription } ).joinWithSeparator(", "))
            }
        }])
        
        self.addWord(.Verb, keyword: "look", alternatives: ["examine"])
        self.addRule(["look"], conditions: [], actions: [{ (command) in
            self.delegate?.chapterShouldPrintLocation(self)
        }])
        
        self.addRule(["look *"], conditions: [], actions: [{ (command) in
            let keyword = command.componentsSeparatedByString(" ")[1]
            let matchingItems: [Item] = self.items.filter( { $0.keyword == keyword && ($0.location == self.inventory || $0.location == self.currentLocation) } )
            if matchingItems.count > 0 {
                for item in matchingItems {
                    self.delegate?.chapter(self, shouldWrite: item.longDescription)
                }
            } else {
                self.delegate?.chapter(self, shouldWrite: "There's nothing to see.)")
            }
        }])
        
        self.addWord(.Verb, keyword: "go")
        self.addRule(["go *"], conditions: [], actions: [{ (command) in
            let keywords = command.componentsSeparatedByString(" ")
            guard keywords.count > 1 else {
                // Not enough recognised input.
                self.delegate?.chapter(self, shouldWrite: "Go where?")
                return
            }
            let keyword = keywords[1]
            if keywords.count == 2 {
                // Check for directions.
                if let direction = self.directions.filter( { $0.isMatch(keyword) }).first {
                    if self.performDirection(Direction(rawValue: direction.keyword)!) == true {
                        self.delegate?.chapterShouldPrintLocation(self)
                        return
                    }
                }
            }
            
            let locationGraph = LocationGraph()
            guard let path = locationGraph.pathToLocationWithName(keyword, currentLocation: self.currentLocation) else {
                self.delegate?.chapter(self, shouldWrite: "You don't know how to get to \(keyword).")
                return
            }
            self.currentLocation = path.last!
            self.delegate?.chapterShouldPrintLocation(self)
        }])
    }
    
    func addWord(type: WordType, keyword: String, alternatives: [String]? = nil) {
        self._words.append(Word(type: type, keyword: keyword, alternatives: alternatives))
    }
    
    func addRule(commands: [String], conditions: [() -> Bool], actions: [(command: String) -> ()]) {
        self.rules.append(Rule(commands: commands, conditions: conditions, actions: actions))
    }
    
}
