//
//  Exit.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 25/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

enum Direction: String {
    case North = "n"
    case East  = "e"
    case South = "s"
    case West  = "w"
    case Up = "u"
    case Down = "d"
    case In = "i"
    case Out = "o"
    
    var name: String {
        switch self {
        case .North: return "North"
        case .East:  return "East"
        case .South: return "South"
        case .West:  return "West"
        case .Up: return "Up"
        case .Down: return "Down"
        case .In: return "In"
        case .Out: return "Out"
        }
    }
}

class Exit: Equatable {
    let direction: Direction
    let location: Location
    init(direction: Direction, location: Location) {
        self.direction = direction
        self.location = location
    }
}

func == (lhs: Exit, rhs: Exit) -> Bool {
    return (lhs.direction == rhs.direction) && (lhs.location == rhs.location)
}

class Location: Equatable, Hashable {
    var name: String
    var description: String
    var exits = [Exit]()
    var rules = [Rule]()
    init(name: String, description: String) {
        self.name = name
        self.description = description
    }
    var hashValue: Int {
        return self.name.hashValue
    }
}

func == (lhs: Location, rhs: Location) -> Bool {
    return (lhs.name == rhs.name)
        && (lhs.description == rhs.description)
        && (lhs.exits == rhs.exits)
        && (lhs.rules == rhs.rules)
}

class Rule: Equatable {
    typealias RuleCondition = () -> Bool
    typealias RuleAction = (command: String) -> ()
    
    let commands: [String]
    let conditions: [RuleCondition]
    let actions: [RuleAction]
    
    func performCommand(testCommand: String) -> Bool {
        for command in self.commands {
            if self.compareCommand(testCommand, rule: command) && self.testConditions() {
                self.performActions(testCommand)
                return true
            }
        }
        return false
    }
    
    func compareCommand(input: String, rule: String) -> Bool {
        if rule.lowercaseString.rangeOfString("*") == nil {
            return input == rule
        }
        let inputTokens = input.componentsSeparatedByString(" ")
        let ruleTokens = rule.componentsSeparatedByString(" ")
        if inputTokens.first != ruleTokens.first {
            return false
        }
        // TODO: Re-evaluate this, I'm not sure it works.
        for var i = 0; i < inputTokens.count; ++i {
            if (i >= ruleTokens.count || ruleTokens.last! != "*") && ruleTokens[i] != "*" && inputTokens[i] != ruleTokens[i] {
                return false
            }
        }
        return true
    }
    
    func testConditions() -> Bool {
        for condition in self.conditions {
            if !condition() {
                return false
            }
        }
        return true
    }
    
    func performActions(command: String) {
        self.actions.forEach( { $0(command: command) } )
    }
    
    init(commands: [String], conditions: [RuleCondition], actions: [RuleAction]) {
        self.commands = commands
        self.conditions = conditions
        self.actions = actions
    }
}

func == (lhs: Rule, rhs: Rule) -> Bool {
    return (lhs.commands == rhs.commands)
}

class Item: Equatable {
    let keyword: String
    let shortDescription: String
    private let _longDescription: String
    var longDescription: String {
        var description = self._longDescription
        if self.variables != nil {
            for (key, value) in self.variables! {
                description = description.stringByReplacingOccurrencesOfString("@{\(key)}", withString: value())
            }
        }
        return description
    }
    // TODO: This is a weird inverse relationship. Perhaps put an Item array on Location instead.
    let location: Location
    let variables: [String: () -> String]?
    
    init(keyword: String, shortDescription: String, longDescription: String, location: Location, variables: [String: () -> String]? = nil) {
        self.keyword = keyword
        self.shortDescription = shortDescription
        self._longDescription = longDescription
        self.location = location
        self.variables = variables
    }
}

func == (lhs: Item, rhs: Item) -> Bool {
    return (lhs.keyword.hashValue == rhs.keyword.hashValue)
}

enum WordType {
    case Direction
    case Verb
    case Noun
}

class Word {
    let type: WordType
    let keyword: String
    let alternatives: [String]?
    
    func isMatch(word: String) -> Bool {
        if word == self.keyword || self.alternatives?.contains(word) == true {
            return true
        }
        return false
    }
    
    init(type: WordType, keyword: String, alternatives: [String]? = nil) {
        self.type = type
        self.keyword = keyword
        self.alternatives = alternatives
    }
}
