//
//  AbstractWorld.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 25/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

protocol ChapterDelegate: class {
    func chapterShouldPrintLocation(chapter: Chapter)
    func chapter(chapter: Chapter, shouldWrite message: String)
}

protocol Chapter: class {
    var currentLocation: Location! { get set }
    var inventory: Location { get set }
    
    var items: [Item] { get }
    
    var directions: [Word] { get }
    var verbs: [Word] { get }
    var nouns: [Word] { get }
    var words: [Word] { get }
    
    var rules: [Rule] { get }
    
    var delegate: ChapterDelegate? { get set }
}

extension Chapter {
    
    func performDirection(direction: Direction) -> Bool {
        if let index = self.currentLocation.exits.indexOf( { $0.direction == direction } ) {
            let exit = self.currentLocation.exits[index]
            self.currentLocation = exit.location
            return true
        }
        return false
    }
    
    func findKeyword(testWord: String) -> String? {
        if let index = self.words.indexOf( { $0.isMatch(testWord) } ) {
            return self.words[index].keyword
        }
        return nil
    }
    
    func hasItem(itemName: String) -> Bool {
        return self.items.contains( { $0.keyword.lowercaseString == itemName } )
    }
    
}
