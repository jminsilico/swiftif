import Foundation

class Chapter0: BaseChapter {
    
    init() {
        let inventory = Location(name: "", description: "")
        super.init(inventory: inventory)
        
        self.addWord(.Direction, keyword: "n", alternatives: ["north"])
        self.addWord(.Direction, keyword: "s", alternatives: ["south"])
        self.addWord(.Verb, keyword: "open")
        self.addWord(.Noun, keyword: "door")
        self.addWord(.Noun, keyword: "coins", alternatives: ["coin", "money", "cash"])
        self.addWord(.Verb, keyword: "swallow", alternatives: ["eat"])
        self.addWord(.Noun, keyword: "cell")
        self.addWord(.Noun, keyword: "corridor")
        
        let cell = Location(name: "Cell", description: "A small prison cell.")
        self.currentLocation = cell
        
        let corridor = Location(name: "Corridor", description: "A narrow corridor.")
        
        let cellToCorridorExit = Exit(direction: Direction.North, location: corridor)
        
        let corridorToCellExit = Exit(direction: Direction.South, location: cell)
        corridor.exits.append(corridorToCellExit)
        
        self.addRule(["open door"], conditions: [], actions: [
            { (command) in
                if cell.exits.contains(cellToCorridorExit) {
                    self.delegate?.chapter(self, shouldWrite: "The door is already open.")
                    return
                }
                self.delegate?.chapter(self, shouldWrite: "You open the door")
                cell.exits.append(cellToCorridorExit)
            }
        ])
        
        var coins = 75
        let coinsItem = Item(keyword: "coins", shortDescription: "Some coins.", longDescription: "@{amount} cents in random coins.", location: cell, variables: ["amount": { String(coins) }])
        self.items.append(coinsItem)
        
        self.addRule(["swallow coins"], conditions: [{ self.currentLocation == coinsItem.location }], actions: [
            { [weak self] (command) in
                coins = Int(arc4random_uniform(UInt32(coins - 1)))
                self?.delegate?.chapter(self!, shouldWrite: "You swallow a handfull of coins.")
                if coins < 30 {
                    self?.delegate?.chapter(self!, shouldWrite: "You are now dead due to greed.")
                    abort()
                }
            }
        ])
    }
    
}
