//
//  Pathfinding.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 28/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

/// A LocationGraph can be used to find a path from one Location to a another based on the name of the Location.
class LocationGraph {
    
    private func neighbors(key: Location) -> [Location] {
        return key.exits.map( { $0.location } )
    }
    
    init() {}
    
    private func breadthFirstSearch(start: Location, goal: String) -> (cameFrom: [Location: Location], goal: Location?) {
        let goalName = goal.lowercaseString
        
        var frontier = [Location]()
        frontier.append(start)
        var cameFrom = [Location: Location]()
        var goalLocation: Location?
        
        while !frontier.isEmpty {
            let current = frontier.removeFirst()
            if current.name.lowercaseString == goalName {
                goalLocation = current
                break
            }
            
            for next in self.neighbors(current) {
                if cameFrom[next] == nil {
                    frontier.append(next)
                    cameFrom[next] = current
                }
            }
        }
        
        return (cameFrom: cameFrom, goal: goalLocation)
    }
    
    private func shortestPath(cameFrom: [Location: Location], start: Location, goal: Location) -> [Location]? {
        guard !cameFrom.isEmpty && cameFrom[goal] != nil else {
            return nil
        }
        var current = goal
        var path = [current]
        while current != start {
            current = cameFrom[current]!
            path.append(current)
        }
        let reversedPath = path.reverse() as [Location]
        return reversedPath
    }
    
    func pathToLocationWithName(goal: String, currentLocation: Location) -> [Location]? {
        let (cameFrom, goalLocation) = self.breadthFirstSearch(currentLocation, goal: goal)
        guard goalLocation != nil else {
            return nil
        }
        let shortestPath = self.shortestPath(cameFrom, start: currentLocation, goal: goalLocation!)
        return shortestPath
    }
    
}
