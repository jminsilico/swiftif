//
//  World.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 25/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

protocol WorldDelegate: class {
    func worldShouldPrintLocation(world: World)
    func world(world: World, shouldWrite message: String)
}

class World {
    
    weak var delegate: WorldDelegate?
    
    func write(message: String) {
        if let delegate = self.delegate {
            delegate.world(self, shouldWrite: message)
            return
        }
        
        print(message)
    }
    
    func printLocation() {
        if let delegate = self.delegate {
            delegate.worldShouldPrintLocation(self)
            return
        }
        
        print("--- \(self.currentChapter.currentLocation.name)")
        print(self.currentChapter.currentLocation.description)
        if self.currentChapter.currentLocation.exits.count > 0 {
            let exits = self.currentChapter.currentLocation.exits.map( { $0.direction.name } ).joinWithSeparator(", ")
            print("Available exits: \(exits)")
        }
    }
    
    var currentChapter: Chapter {
        didSet {
            self.currentChapter.delegate = self
        }
    }
    
    func action(inputString: String) {
        if inputString.characters.count == 0 {
            return
        }
        let sanitisedInputString = inputString.sanitizedString()
        var words = sanitisedInputString.lowercaseString.componentsSeparatedByString(" ")
        
        var foundWords: String?
        var foundVerb = false
        var foundNouns = 0
        
        words = words.flatMap( { self.currentChapter.findKeyword($0) } )
        for word in words {
            // Direction check.
            if !foundVerb && self.currentChapter.directions.contains( { $0.isMatch(word) } ) {
                if self.currentChapter.performDirection(Direction(rawValue: word)!) == true {
                    self.printLocation()
                } else {
                    self.write("You cannot go that way.")
                }
                return
            } else if !foundVerb && self.currentChapter.verbs.contains( { $0.isMatch(word) } ) {
                foundWords = word
                foundVerb = true
            } else if foundVerb == true
                && foundNouns < 2
                && (self.currentChapter.nouns.contains( { $0.isMatch(word) } ) == true || self.currentChapter.hasItem(word)) {
                foundWords = foundWords! + " " + word
                ++foundNouns
            }
        }
        
        if !foundVerb {
            self.write("I don't understand.")
            return
        }
        
        // Check for global rules.
        for rule in self.currentChapter.rules {
            if rule.performCommand(foundWords!) {
                return
            }
        }
        
        // Check for local rules in the current location.
        for rule in self.currentChapter.currentLocation.rules {
            if rule.performCommand(foundWords!) {
                return
            }
        }
        
        self.write("I don't understand.")
    }
    
    init(chapter: Chapter) {
        self.currentChapter = chapter
        self.currentChapter.delegate = self
    }
    
}

extension World: ChapterDelegate {
    
    func chapter(chapter: Chapter, shouldWrite message: String) {
        self.write(message)
    }
    
    func chapterShouldPrintLocation(chapter: Chapter) {
        self.printLocation()
    }
    
}
