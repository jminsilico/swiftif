//
//  ViewController.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 25/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var prompt: UILabel!
    @IBOutlet var input: UITextField!
    @IBOutlet var output: UITextView!
    
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    var maxNumberOfCharacters = 50
    var allowedCharacters = "1234567890abcdefghijklmnopqrstuvwxyz "
    
    let chapter = DreamwebChapter()
    var world: World!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerForKeyboardNotifications()
        
        self.output.text = "\n"
        for _ in 1...32 {
            self.output.text = self.output.text + "\n"
        }
        
        self.input.becomeFirstResponder()
        
        self.world = World(chapter: self.chapter)
        self.world.delegate = self
        
//        let actions = ["N", "dfgsdfgs", "open", "open door", "open door", "n", "s", "look coins", "swallow money", "look at coins"]
//        actions.forEach { world.action($0) }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.worldShouldPrintLocation(self.world)
    }
    
    func processAction() {
        let inputString = self.input.text!
        self.world.action(inputString)
        self.input.text = ""
    }
    
}

extension ViewController: UITextFieldDelegate {
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        textField.text = (textField.text! + string).sanitizedString().uppercaseString
        return false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.world(self.world, shouldWrite: ">" + self.input.text!)
        self.processAction()
        return false
    }
    
}

extension ViewController: WorldDelegate {
    func world(world: World, shouldWrite message: String) {
        self.output.text = self.output.text + "\n" + message
        self.output.scrollToBottom()
    }
    
    func worldShouldPrintLocation(world: World) {
        self.output.text = self.output.text + "\n" + world.currentChapter.currentLocation.name
        self.output.text = self.output.text + "\n" + world.currentChapter.currentLocation.description
        if world.currentChapter.currentLocation.exits.count > 0 {
            let exits = world.currentChapter.currentLocation.exits.map( { $0.direction.name } ).joinWithSeparator(", ")
            self.output.text = self.output.text + "\n" + "Available exits: \(exits)"
        }
        self.output.scrollToBottom()
    }
}

// MARK: - Keyboard

extension ViewController {
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidChangeFrameNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        let info: [NSObject: AnyObject] = notification.userInfo!
        
        if let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() {
            var keyboardHeight: CGFloat = 0.0
            if keyboardSize.height < keyboardSize.width {
                keyboardHeight = keyboardSize.height
            } else {
                keyboardHeight = keyboardSize.width
            }
            self.bottomConstraint.constant = keyboardHeight
//            UIView.animateWithDuration(0) {
                self.view.layoutIfNeeded()
                self.output.scrollToBottom()
//            }
        }
        self.calculateOutputSize()
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        self.bottomConstraint.constant = 0
//        UIView.animateWithDuration(0) {
            self.view.layoutIfNeeded()
            self.output.scrollToBottom()
//        }
        self.calculateOutputSize()
    }
    
    func calculateOutputSize() {
        print(self.output.bounds.size)
    }
}

extension UITextView {
    func scrollToBottom() {
        self.scrollRectToVisible(CGRect(x: 0, y: self.contentSize.height - 1, width: self.contentSize.width, height: 1), animated: false)
        self.scrollEnabled = false
        self.scrollEnabled = true
    }
}
