//
//  Direction.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

enum DirectionStatus {
    case ok
    case invalid
    case blocked(reason: String?)
}

enum Direction: String {
    case north = "n"
    case east  = "e"
    case south = "s"
    case west  = "w"
    case up = "u"
    case down = "d"
    case into = "i"
    case out = "o"
    case northEast = "ne"
    case northWest = "nw"
    case southEast = "se"
    case southWest = "sw"
    
    var name: String {
        switch self {
        case .north: return "North"
        case .east:  return "East"
        case .south: return "South"
        case .west:  return "West"
        case .up: return "Up"
        case .down: return "Down"
        case .into: return "In"
        case .out: return "Out"
        case .northEast: return "NorthEast"
        case .northWest: return "NorthWest"
        case .southEast: return "SouthEast"
        case .southWest: return "SouthWest"
        }
    }
}
