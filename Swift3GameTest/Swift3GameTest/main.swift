//
//  main.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 19/10/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation
import AppKit
//import AVFoundation
//
//let gameBundle = GameDataBundleTest()
//let data = gameBundle.test()!
//let player = try! AVAudioPlayer(data: data)
//player.prepareToPlay()
//player.play()

let promptOption = Option(names: ["p", "prompt"], argumentCount: 1, command: { (option) in
    //promptString = option
})
process(options: [promptOption], arguments: Process().arguments)

let chapterM2 = ChapterM2()
let chapterSecondFloor = ChapterSecondFloor()
var world = World(chapters: ["intro": chapterM2, "secondFloor": chapterSecondFloor], initialChapterId: "secondFloor")
//world.printLocation()

func waitForInput() {
    waitingForInput: while true {
        if let inputString = readLine() {
            world.action(inputString: inputString)
        }
    }
}

//print("\u{001B}[0;33myellow")
world.printPrompt()
waitForInput()
