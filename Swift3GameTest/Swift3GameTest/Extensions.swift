//
//  Extensions.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 28/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

extension String {
    var sanitized: String {
        let chars = Set<Character>(" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".characters)
        return String(self.characters.filter( { chars.contains($0) } ))
    }
    
    var quote: String {
        return "\"\(self)\""
    }
}

extension Array where Element: Equatable {
    /// Remove all instances of an object from the array.
    mutating func remove(object: Element) {
        while let index = index(of: object) {
            remove(at: index)
        }
    }
}

extension Array {
    var sample: Element? {
        guard self.count > 0 else {
            return nil
        }
        let randomIndex = Int(arc4random()) % count
        return self[randomIndex]
    }
}
