//
//  Word.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

enum WordType {
    case direction
    case verb
    case noun
}

class Word {
    let type: WordType
    let keyword: String
    let alternatives: [String]?
    
    func isMatch(word: String) -> Bool {
        if word == self.keyword || self.alternatives?.contains(word) == true {
            return true
        }
        return false
    }
    
    init(type: WordType, keyword: String, alternatives: [String]? = nil) {
        self.type = type
        self.keyword = keyword
        self.alternatives = alternatives
    }
}
