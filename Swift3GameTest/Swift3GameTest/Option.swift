//
//  Option.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 26/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

struct Option {
    let names: [String]
    let argumentCount: Int
    let command: (_ option: String) -> ()
    var description: String {
        return "[\(self.names.map( { "-\($0)" } ).joined(separator: "|"))]"
    }
}

func process(options: [Option], arguments: [String]?) {
    guard let arguments = arguments else {
        return
    }
    var options = options
    options.insert(Option(names: ["h", "help"], argumentCount: 0, command: { (option) -> () in
        let commandName = URL(string: Process().arguments![0])!.lastPathComponent
        let optionsString = options.reduce("", { $0 + " " + $1.description })
        print("usage: \(commandName)\(optionsString)")
        abort()
    }), at: 0)
    argumentIterator: for i in 0 ..< arguments.count {
        let argument = arguments[i].trimmingCharacters(in: CharacterSet(charactersIn: "-"))
        for option in options {
            if option.names.contains(argument) && i + option.argumentCount < arguments.count {
                option.command(arguments[i + option.argumentCount])
                continue argumentIterator
            }
        }
    }
}

//if let index = Process.arguments.indexOf( { $0 == "-prompt" || $0 == "-p" } ) where index + 1 < Process.arguments.count {
//    promptString = Process.arguments[index + 1] + " "
//}
