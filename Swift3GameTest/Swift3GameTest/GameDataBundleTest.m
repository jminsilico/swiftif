//
//  GameDataBundleTest.m
//  GameDataBundleTest
//
//  Created by Jonathan McAllister on 26/10/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

#import "GameDataBundleTest.h"
#import "test.mp3.h"

@implementation GameDataBundleTest

- (NSData *)test
{
    unsigned char *mp3Bytes = noise_extra_loop_mp3;
    NSUInteger mp3Length = noise_extra_loop_mp3_len;
    
    // make NSData and UIImage from it
    NSData *mp3Data = [NSData dataWithBytesNoCopy: mp3Bytes length: mp3Length freeWhenDone: NO];
    return mp3Data;
}

@end
