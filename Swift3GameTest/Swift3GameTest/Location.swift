//
//  Location.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class Location: Equatable, Hashable, Rules {
    var name: String
    var description: String {
        return self.descriptionVariables.flatMap( { $0.value() } ).joined(separator: " ")
    }
    // TODO: Do we need the key?
    var descriptionVariables = [(key: String, value: () -> String?)]()
    var exits = [Exit]()
    var rules = [Rule]()
    
    init(name: String) {
        self.name = name
    }
    var hashValue: Int {
        return self.name.hashValue
    }
    
    static func == (lhs: Location, rhs: Location) -> Bool {
        return (lhs.name == rhs.name)
            && (lhs.exits == rhs.exits)
            && (lhs.rules == rhs.rules)
    }
}
