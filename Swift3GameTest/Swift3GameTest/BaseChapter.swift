//
//  Chapter1.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 26/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

class BaseChapter: Chapter {
    
    weak var delegate: ChapterDelegate?
    
    var player: Actor
    
    var items = [Item]()
    var actors = [Actor]()
    
    var directions: [Word] {
        return self._words.filter( { $0.type == .direction } )
    }
    var verbs: [Word] {
        return self._words.filter( { $0.type == .verb } )
    }
    var nouns: [Word] {
        return self._words.filter( { $0.type == .noun } )
    }
    private var _words = [Word]()
    var words: [Word] {
        return self._words
    }
    
    var rules = [Rule]()
    
    var turnIncrement: ((Int) -> Void)?
    var canPerformDirection: ((Direction) -> DirectionStatus)?
    var enterChapter: (() -> ())?
    
    var prompt = "What now? > "
    
    var activeMenu: Menu? {
        didSet {
            if let menuText = self.activeMenu?.menu {
                self.write(menuText)
            }
        }
    }
    
    init(player: Actor) {
        self.player = player
        
        self.addWord(type: .verb, keyword: "inventory", alternatives: ["i", "inv"])
        self.addRule(commands: ["inventory"], conditions: [], actions: [{ (command) in
            self.delegate?.chapter(chapter: self, shouldWrite: "You have:")
            let inventoryItems = self.items.filter( { $0.location == self.player.inventory } )
            if inventoryItems.count == 0 {
                let randomItem = ["Your health.", "All the time in the world.", "No tea.", "A splitting headache"].sample!
                self.delegate?.chapter(chapter: self, shouldWrite: randomItem)
            } else {
                self.delegate?.chapter(chapter: self, shouldWrite: inventoryItems.map( { $0.shortDescription } ).joined(separator: ", "))
            }
        }])
        
        self.addWord(type: .verb, keyword: "look", alternatives: ["examine"])
        self.addRule(commands: ["look"], conditions: [], actions: [{ (command) in
            self.delegate?.chapterShouldPrintLocation(chapter: self)
        }])
        
        self.addRule(commands: ["look *"], conditions: [], actions: [{ (command) in
            let keyword = command.components(separatedBy: " ")[1]
            let matchingItems: [Item] = self.items.filter( { $0.keyword == keyword && ($0.location == self.player.inventory || $0.location == self.player.location!) } )
            if matchingItems.count > 0 {
                for item in matchingItems {
                    self.delegate?.chapter(chapter: self, shouldWrite: item.longDescription)
                }
            } else {
                self.delegate?.chapter(chapter: self, shouldWrite: "There's nothing to see.")
            }
        }])
        
        self.addWord(type: .verb, keyword: "talk")
        self.addRule(commands: ["talk"], conditions: [], actions: [{ (command) in
            self.delegate?.chapter(chapter: self, shouldWrite: "To whom?")
        }])
        self.addRule(commands: ["talk *"], conditions: [], actions: [{ (command) in
            let keyword = command.components(separatedBy: " ")[1].lowercased()
            guard let actor = self.actors.filter( { $0.key.lowercased() == keyword && ($0.location != nil && $0.location! == self.player.location!) } ).first else {
                self.delegate?.chapter(chapter: self, shouldWrite: "\(String(keyword.characters.first!).uppercased())\(String(keyword.characters.dropFirst())) is nowhere to be seen.")
                return
            }
            guard let talkMenu = actor.talkMenu else {
                self.delegate?.chapter(chapter: self, shouldWrite: "There's nothing to be said.")
                return
            }
            self.activeMenu = talkMenu
        }])
        
        self.addWord(type: .verb, keyword: "take", alternatives: ["get"])
        self.addRule(commands: ["take"], conditions: [], actions: [{ (command) in
            self.delegate?.chapter(chapter: self, shouldWrite: "Take what?")
        }])
        self.addRule(commands: ["take *"], conditions: [], actions: [{ (command) in
            let keyword = command.components(separatedBy: " ")[1].lowercased()
            guard let item = self.items.filter( { $0.keyword.lowercased() == keyword && ($0.location != nil && $0.location! == self.player.location!) } ).first else {
                self.delegate?.chapter(chapter: self, shouldWrite: "There is no \(keyword) here.")
                return
            }
            self.write("You take the \(keyword).")
            item.location = self.player.inventory
        }])
        
        self.addWord(type: .verb, keyword: "go")
        self.addRule(commands: ["go *"], conditions: [], actions: [{ (command) in
            let keywords = command.components(separatedBy: " ")
            guard keywords.count > 1 else {
                // Not enough recognised input.
                self.delegate?.chapter(chapter: self, shouldWrite: "Go where?")
                return
            }
            let keyword = keywords[1]
            if keywords.count == 2 {
                // Check for directions.
                if let direction = self.directions.filter( { $0.isMatch(word: keyword) }).first {
                    if self.performDirection(direction: Direction(rawValue: direction.keyword)!) == true {
                        self.delegate?.chapterShouldPrintLocation(chapter: self)
                        return
                    }
                }
            }
            
            let locationGraph = LocationGraph()
            guard let path = locationGraph.pathToLocationWithName(goal: keyword, currentLocation: self.player.location!) else {
                self.delegate?.chapter(chapter: self, shouldWrite: "You don't know how to get to \(keyword).")
                return
            }
            self.player.location = path.last!
            self.delegate?.chapterShouldPrintLocation(chapter: self)
        }])
    }
    
    func addWord(type: WordType, keyword: String, alternatives: [String]? = nil) {
        self._words.append(Word(type: type, keyword: keyword, alternatives: alternatives))
    }
    
}
