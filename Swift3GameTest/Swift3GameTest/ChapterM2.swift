//
//  ChapterM4.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 20/10/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class ChapterM2: BaseChapter {
    
    init() {
        let player = Actor(key: "you", name: "You")
        super.init(player: player)
        
        self.addWord(type: .direction, keyword: "n", alternatives: ["north"])
        self.addWord(type: .direction, keyword: "s", alternatives: ["south"])
        self.addWord(type: .verb, keyword: "open")
        self.addWord(type: .verb, keyword: "close")
        self.addWord(type: .verb, keyword: "throw")
        self.addWord(type: .verb, keyword: "use")
        self.addWord(type: .verb, keyword: "hide")
        self.addWord(type: .verb, keyword: "take", alternatives: ["get"])
        self.addWord(type: .verb, keyword: "drop")
        self.addWord(type: .verb, keyword: "help")
        self.addWord(type: .verb, keyword: "yes")
        self.addWord(type: .verb, keyword: "no")
        
        self.addWord(type: .noun, keyword: "door")
        self.addWord(type: .noun, keyword: "window")
        self.addWord(type: .noun, keyword: "curtains", alternatives: ["curtain"])
        self.addWord(type: .noun, keyword: "whiteboard", alternatives: ["board"])
        self.addWord(type: .noun, keyword: "table")
        self.addWord(type: .noun, keyword: "tv")
        self.addWord(type: .noun, keyword: "computer", alternatives: ["laptop"])
        self.addWord(type: .noun, keyword: "body", alternatives: ["bendix", "corpse"])
        self.addWord(type: .noun, keyword: "cupboard")
        self.addWord(type: .noun, keyword: "phone", alternatives: ["smartphone"])
        self.addWord(type: .noun, keyword: "martin")
        
        let m2 = Location(name: "M2")
        self.player.location = m2
        
        let hallway = Location(name: "Hallway")
        
        let doorFromM2ToHallway = Exit(direction: .north, location: hallway)
        
        self.addRule(commands: ["n"], conditions: [], actions: [{ (command) in
            print(command)
        }])
        
        
        enum BodyLocation {
            case seat
            case underTable
            case behindCurtain
            case inCupboard
        }
        var bodyLocation: BodyLocation = .seat
        var curtainsAreOpen = true
        var windowIsOpen = true
        var doorIsOpen = false
        var cupboardIsOpen = false
        
        let tv = Item(keyword: "tv", shortDescription: "A large tv hanging on the wall.", longDescription: "A large tv hanging on the wall. It doesn't seem to work.", location: m2)
        self.items.append(tv)
        
        let whiteboard = Item(keyword: "whiteboard", shortDescription: "Just a whiteboard.", longDescription: "A whiteboard with strange scriptings on it. There is a diagram with the title 'Ports and Adapters'. It makes absolutely no sense.", location: m2)
        self.items.append(whiteboard)
        
        let table = Item(keyword: "table", shortDescription: "Table", longDescription: "@{isBodyHiddenUnderTable}", location: m2, variables: [
            "isBodyHiddenUnderTable": {
                if bodyLocation == .underTable {
                    return "A table made of plastic... There is a body under it! How strange."
                }
                
                return "A table made of plastic... I think it's from IKEA."
            }
        ])
        self.items.append(table)
        
//        NSWorkspace.shared().launchApplication("/Applications/Photo Booth.app")
        
        self.addRule(commands: ["use phone"], conditions: [], actions: [{ (command) in
            self.write("You snap a gorgeous selfie.")
        }])
        
        self.addRule(commands: ["use phone *"], conditions: [], actions: [{ (command) in
            let item = command.components(separatedBy: " ").last!
            self.write("You selfie with the \(item).")
        }])
        
        let body = Item(keyword: "body", shortDescription: "Bendix (DECEASED)", longDescription: "@{isHidden}", location: m2, variables: [
            "isHidden": {
                switch bodyLocation {
                case .behindCurtain:
                    guard curtainsAreOpen else {
                        return "What body?"
                    }
                    return "The slowly decomposing corpse of your beloved colleague Bendix. He looks like he wants to say 'look inwards, and fuck off'"
                case .inCupboard:
                    return "Body parts are sticking out of the cupboard. At least his head is hidden."
                case .underTable:
                    return "He looks so peaceful, like he is taking a nap."
                case .seat:
                    return "The slowly decomposing corpse of your beloved colleague Bendix."
                }
            }
        ])
        self.items.append(body)
        self.addRule(commands: ["take body"], conditions: [], actions: [{ (command) in
            self.write(["He's too heavy to carry around!  Too many burgers at lunch.", "He doesn't fit in my pocket"].sample!)
        }])
        self.addRule(commands: ["hide body"], conditions: [], actions: [{ (command) in
            self.write("But where?")
        }])
        self.addRule(commands: ["hide body curtains"], conditions: [], actions: [{ (command) in
            switch bodyLocation {
            case .behindCurtain:
                self.write("Body is already behind the curtains.")
            case .inCupboard:
                self.write("You drag the dead weight of Bendix from the cupboard to behind the curtains and close them tightly. He's finally out of the closet!")
            case .underTable:
                self.write("You drag the dead weight of Bendix from the under the table to behind the curtains and close them tightly.")
            case .seat:
                self.write("Great idea! You drag the dead weight of Bendix behind the curtains and close them tightly.")
            }
            bodyLocation = .behindCurtain
            curtainsAreOpen = false
        }])
        self.addRule(commands: ["hide body cupboard"], conditions: [], actions: [{ (command) in
            switch bodyLocation {
            case .seat:
                self.write("You put the body in the cupboard. It doesn't quite fit.")
            case .behindCurtain:
                self.write("You open the curtains and carry the body from the window to the cupboard... jeezz what did he have for lunch!")
                curtainsAreOpen = true
            case .underTable:
                self.write("You drag him from under the table... God he is heavy!... and stuff him into the cupboard.")
            case .inCupboard:
                self.write("The body is already there!")
            }
            
            bodyLocation = .inCupboard
            cupboardIsOpen = true
        }])
        self.addRule(commands: ["hide body table"], conditions: [], actions: [{ (command) in
            switch bodyLocation {
            case .seat:
                self.write("You posistion the body under the table.")
            case .behindCurtain:
                self.write("You open the curtains and carry the body from the window to under the table... You already miss this guy!")
                curtainsAreOpen = true
            case .underTable:
                self.write("That is where the body is!")
            case .inCupboard:
                self.write("You pull the body out of the cupboard and plant it under the table. Maybe no one will notice.")
            }
            
            bodyLocation = .underTable
        }])
        
        self.addRule(commands: ["help"], conditions: [], actions: [ { (command) in
            if bodyLocation != .behindCurtain {
                self.write(["I'd hide that body!", "You should get out of here, if only there wasn't a body lying around."].sample!)
            }
        }])
        
        let phone = Item(keyword: "phone", shortDescription: "Your phone", longDescription: "A smartphone. It reads @{time}. If only you could remember the code.", location: self.player.inventory, variables: ["time": { DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: DateFormatter.Style.short) }])
        self.items.append(phone)
        
        let computer = Item(keyword: "computer", shortDescription: "A laptop", longDescription: "A shiny Mac laptop, totally drained of power.", location: m2)
        self.items.append(computer)
        self.addRule(commands: ["take computer"], conditions: [{
            return computer.location == self.player.location! || computer.location == self.player.inventory
        }], actions: [{ (command) in
            guard computer.location != self.player.inventory else {
                self.write("You're already carrying the computer.")
                return
            }
            self.write("You take the computer.")
            computer.location = self.player.inventory
        }])
        self.addRule(commands: ["use computer"], conditions: [{
            return computer.location == self.player.location! || computer.location == self.player.inventory
        }], actions: [{ (command) in
            self.write("Bendix isn't the only dead thing in the room.")
        }])
        
        
        let curtains = Item(keyword: "curtains", shortDescription: "", longDescription: "The curtains are @{isOpen}.", location: m2, variables: ["isOpen": { curtainsAreOpen ? "open" : "closed" }])
        self.items.append(curtains)
        self.addRule(commands: ["open curtains"], conditions: [], actions: [
            { (command) in
                guard !curtainsAreOpen else {
                    self.write("The curtains are already open.")
                    return
                }
                if bodyLocation == .behindCurtain {
                    //TODO: change it
                    self.write("You open the curtains. The dead body is not hidden anymore.")
                } else {
                    self.write("You open the curtains. Light streams through the window, burning your retinas clean off.")
                }
                curtainsAreOpen = true
            }
        ])
        self.addRule(commands: ["close curtains"], conditions: [], actions: [
            { (command) in
                guard curtainsAreOpen else {
                    self.write("The curtains are already closed.")
                    return
                }
                self.write("You close the curtains. The room is plunged in to darkness.")
                curtainsAreOpen = false
            }
        ])
        
        
        let window = Item(keyword: "window", shortDescription: "", longDescription: "@{isOpen}", location: m2, variables: ["isOpen": {
            guard curtainsAreOpen else {
                return "They're behind the closed curtains."
            }
            return windowIsOpen ? "The window is open." : "The window is closed."
        }])
        self.items.append(window)
        self.addRule(commands: ["open window"], conditions: [], actions: [
            { (command) in
                guard curtainsAreOpen else {
                    self.write("You'll have to open the curtains first.")
                    return
                }
                guard !windowIsOpen else {
                    self.write("The window is already open.")
                    return
                }
                self.write("You open the window. It's still extemely warm in here.")
                windowIsOpen = true
            }
        ])
        self.addRule(commands: ["close window"], conditions: [], actions: [
            { (command) in
                guard curtainsAreOpen else {
                    self.write("You'll have to open the curtains first.")
                    return
                }
                guard windowIsOpen else {
                    self.write("The window is already closed.")
                    return
                }
                self.write("You close the window. Good luck with breathing.")
                windowIsOpen = false
            }
        ])
        
        let martin = Actor(key: "martin", name: "Martin")
        self.actors.append(martin)
        
        let martinTalkMenuYes = MenuItem(key: "Yes", showKey: false) {
            self.write("You spend two hours listening to Martin explain something about Ports & Adapters.  The body lying in the sun behind the curtains starts to smell. Blah blah blah.")
            self.delegate?.chapterShouldEndGame(chapter: self)
        }
        let martinTalkMenuNo = MenuItem(key: "No", showKey: false) {
            self.write("You escape Martin.")
            self.delegate?.chapter(chapter: self, shouldChangeTo: "secondFloor")
        }
        
        let martinTalkMenu = Menu(question: "Martin: \"Have you got two minutes?\"", items: [martinTalkMenuYes, martinTalkMenuNo])
        
        self.addRule(commands: ["talk martin"], conditions: [], actions: [{ (command) in
            guard doorIsOpen else {
                self.write("Martin isn't around.")
                return
            }
            if bodyLocation == .behindCurtain {
                self.activeMenu = martinTalkMenu
            } else {
                self.write("I should do something about the body first.")
            }
        }])
        
        let door = Item(keyword: "door", shortDescription: "", longDescription: "@{isOpen}", location: m2, variables: ["isOpen": { doorIsOpen ? "The door is open. Martin is standing outside." : "The door is closed." }])
        self.items.append(door)
        self.addRule(commands: ["open door"], conditions: [], actions: [
            { (command) in
                guard !doorIsOpen else {
                    self.write("The door is already open.")
                    return
                }
                self.write("You open the door. Martin is standing outside with his back to you. He hasn't noticed you yet.")
                m2.exits.append(doorFromM2ToHallway)
                doorIsOpen = true
            }
        ])
        self.addRule(commands: ["close door"], conditions: [], actions: [
            { (command) in
                guard doorIsOpen else {
                    self.write("The door is already closed.")
                    return
                }
                self.write("You close the door.")
                if let index = m2.exits.index(of: doorFromM2ToHallway) {
                    m2.exits.remove(at: index)
                }
                doorIsOpen = false
            }
        ])
        
        let cupboard = Item(keyword: "cupboard", shortDescription: "", longDescription: "@{isOpen}", location: m2, variables: ["isOpen": {
            if bodyLocation == .inCupboard {
                return "There is a body sticking out of the cupboard."
            } else {
                return cupboardIsOpen ? "The cupboard is open. It's full of wires." : "The cupboard is closed."
            }
        }])
        self.items.append(cupboard)
        self.addRule(commands: ["open cupboard"], conditions: [], actions: [
            { (command) in
                guard !cupboardIsOpen else {
                    self.write("The cupboard is already open.")
                    return
                }
                self.write("You open the cupboard.")
                cupboardIsOpen = true
            }
        ])
        self.addRule(commands: ["close cupboard"], conditions: [], actions: [
            { (command) in
                guard cupboardIsOpen else {
                    self.write("The cupboard is already closed.")
                    return
                }
                self.write("You close the cupboard.")
                cupboardIsOpen = false
            }
        ])
        
        var behindCurtainsWindowOpenCount = 0
        let bodyFalling = {
            if (bodyLocation == .behindCurtain) && windowIsOpen {
                if behindCurtainsWindowOpenCount == 1 {
                    // TODO: Body falls out of window.
                    self.write("There's a loud crash from outside, like 90kg of marmelade hitting concrete, followed by screams.")
                    self.delegate?.chapterShouldEndGame(chapter: self)
                } else {
                    behindCurtainsWindowOpenCount += 1
                }
                return
            }
        }
        
        self.turnIncrement = { (count) in
            bodyFalling()
            if count == 15 {
                self.write("Martin bursts throught the door.")
                self.delegate?.chapterShouldEndGame(chapter: self)
            }
        }
        
        self.canPerformDirection = { (direction) in
            if self.player.location! == m2 {
                if bodyLocation == .behindCurtain {
                    self.activeMenu = martinTalkMenu
                    return .blocked(reason: nil)
                }
                return .blocked(reason: "Martin is in the way.  I'd better do something about that body before he notices.")
            }
            return .ok
        }
        
        m2.descriptionVariables = [
            (key: "decor", value: { "You awaken in a small meeting room with garish decor." }),
            (key: "curtains", value: {
                if curtainsAreOpen {
                    return "Light streams through a small window with some large heavy dark green curtains."
                }
                return "The curtains are closed and Bendix isn't the only one that's dim around here."
            }),
            (key: "unmovableItems", value: { "You can see a tv on top of a cupboard and a whiteboard on the wall." }),
            (key: "table", value: {
                if computer.location == m2 {
                    return "On a table in the middle of the room lies a computer next to a body (Bendix [DECEASED])."
                }
                return "On a table in the middle of the room lies a body (Bendix [DECEASED])."
            })
        ]
    }
    
}
