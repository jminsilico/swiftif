//
//  WearableItem.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class WearableItem: Item {
    var worn: Bool = false
    override var shortDescription: String {
        get {
            if worn {
                return ("\(super.shortDescription) (worn)")
            }
            return super.shortDescription
        }
        set {
            super.shortDescription = newValue
        }
    }
}
