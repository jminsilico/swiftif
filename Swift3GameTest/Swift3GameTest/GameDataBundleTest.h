//
//  GameDataBundleTest.h
//  GameDataBundleTest
//
//  Created by Jonathan McAllister on 26/10/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameDataBundleTest : NSObject

- (NSData *)test;

@end
