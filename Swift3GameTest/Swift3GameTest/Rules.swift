//
//  Rules.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

protocol Rules: class {
    
    var rules: [Rule] { get set }
    
}

extension Rules {
    
    func addRule(commands: [String], conditions: [() -> Bool], actions: [(_ command: String) -> ()]) {
        let rule = Rule(commands: commands, conditions: conditions, actions: actions)
        if commands.filter({ $0.contains("*")}).count > 0 {
            self.rules.append(rule)
        } else {
            self.rules.insert(rule, at: 0)
        }
    }
    
}
