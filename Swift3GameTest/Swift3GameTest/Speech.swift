//
//  Speach.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 19/10/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation
import AppKit

let speechSynthesizer = NSSpeechSynthesizer(voice: "com.apple.speech.synthesis.voice.samantha")!

func speak(string: String) {
    speechSynthesizer.startSpeaking(string)
}
