//
//  Actor.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class Actor: Equatable {
    let key: String
    let name: String
    var talkMenu: Menu?
    var location: Location?
    let inventory = Location(name: "")
    
    init(key: String, name: String) {
        self.key = key
        self.name = name
    }
    
    static func == (lhs: Actor, rhs: Actor) -> Bool {
        return (lhs.name == rhs.name)
    }
}
