//
//  Exit.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class Exit: Equatable {
    let direction: Direction
    let location: Location
    init(direction: Direction, location: Location) {
        self.direction = direction
        self.location = location
    }
    static func == (lhs: Exit, rhs: Exit) -> Bool {
        return (lhs.direction == rhs.direction) && (lhs.location == rhs.location)
    }
}
