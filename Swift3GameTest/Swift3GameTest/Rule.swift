//
//  Rule.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class Rule: Equatable {
    typealias RuleCondition = () -> Bool
    typealias RuleAction = (_ command: String) -> ()
    
    let commands: [String]
    let conditions: [RuleCondition]
    let actions: [RuleAction]
    
    func performCommand(testCommand: String) -> Bool {
        for command in self.commands {
            if self.compareCommand(input: testCommand, rule: command) && self.testConditions() {
                self.performActions(command: testCommand)
                return true
            }
        }
        return false
    }
    
    func compareCommand(input: String, rule: String) -> Bool {
        if rule.lowercased().range(of: "*") == nil {
            return input == rule
        }
        let inputTokens = input.components(separatedBy: " ")
        let ruleTokens = rule.components(separatedBy: " ")
        if inputTokens.first != ruleTokens.first {
            return false
        }
        // TODO: Re-evaluate this, I'm not sure it works.
        for i in 0 ..< inputTokens.count {
            if i >= ruleTokens.count {
                return false
            }
            if ruleTokens.last! != "*" && ruleTokens[i] != "*" && inputTokens[i] != ruleTokens[i] {
                return false
            }
        }
        return true
    }
    
    func testConditions() -> Bool {
        for condition in self.conditions {
            if !condition() {
                return false
            }
        }
        return true
    }
    
    func performActions(command: String) {
        self.actions.forEach( { $0(command) } )
    }
    
    init(commands: [String], conditions: [RuleCondition], actions: [RuleAction]) {
        self.commands = commands
        self.conditions = conditions
        self.actions = actions
    }
    
    static func == (lhs: Rule, rhs: Rule) -> Bool {
        return (lhs.commands == rhs.commands)
    }
}
