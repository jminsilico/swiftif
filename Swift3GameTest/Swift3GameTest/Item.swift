//
//  Item.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class Item: Equatable {
    let keyword: String
    var shortDescription: String
    private let _longDescription: String
    var longDescription: String {
        var description = self._longDescription
        if self.variables != nil {
            for (key, value) in self.variables! {
                description = description.replacingOccurrences(of: "@{\(key)}", with: value())
            }
        }
        return description
    }
    // TODO: This is a weird inverse relationship. Perhaps put an Item array on Location instead.
    var location: Location?
    let variables: [String: () -> String]?
    
    init(keyword: String, shortDescription: String, longDescription: String, location: Location? = nil, variables: [String: () -> String]? = nil) {
        self.keyword = keyword
        self.shortDescription = shortDescription
        self._longDescription = longDescription
        self.location = location
        self.variables = variables
    }
    
    static func == (lhs: Item, rhs: Item) -> Bool {
        return (lhs.keyword.hashValue == rhs.keyword.hashValue)
    }
}
