import Foundation

class Chapter0: BaseChapter {
    
    init() {
        let player = Actor(key: "you", name: "You")
        super.init(player: player)
//
//        self.addWord(type: .Direction, keyword: "n", alternatives: ["north"])
//        self.addWord(type: .Direction, keyword: "s", alternatives: ["south"])
//        self.addWord(type: .Verb, keyword: "open")
//        self.addWord(type: .Noun, keyword: "door")
//        self.addWord(type: .Noun, keyword: "coins", alternatives: ["coin", "money", "cash"])
//        self.addWord(type: .Verb, keyword: "swallow", alternatives: ["eat"])
//        self.addWord(type: .Noun, keyword: "cell")
//        self.addWord(type: .Noun, keyword: "corridor")
//        
//        let cell = Location(name: "Cell", description: "A small prison cell.  There is a door in the north wall.")
//        self.currentLocation = cell
//        
//        let corridor = Location(name: "Corridor", description: "A narrow corridor.")
//        
//        let cellToCorridorExit = Exit(direction: Direction.North, location: corridor)
//        
//        let corridorToCellExit = Exit(direction: Direction.South, location: cell)
//        corridor.exits.append(corridorToCellExit)
//        
//        self.addRule(commands: ["open door"], conditions: [], actions: [
//            { (command) in
//                if cell.exits.contains(cellToCorridorExit) {
//                    self.delegate?.chapter(chapter: self, shouldWrite: "The door is already open.")
//                    return
//                }
//                self.delegate?.chapter(chapter: self, shouldWrite: "You open the door")
//                cell.description = "A small prison cell.  A door on the north wall lies open."
//                self.delegate?.chapterShouldPrintLocation(chapter: self)
//                cell.exits.append(cellToCorridorExit)
//            }
//        ])
//        
//        var coins = 75
//        let coinsItem = Item(keyword: "coins", shortDescription: "Some coins.", longDescription: "@{amount} cents in random coins.", location: cell, variables: ["amount": { String(coins) }])
//        self.items.append(coinsItem)
//        
//        self.addRule(commands: ["swallow coins"], conditions: [{ self.currentLocation == coinsItem.location }], actions: [
//            { [weak self] (command) in
//                coins = Int(arc4random_uniform(UInt32(coins - 1)))
//                self?.delegate?.chapter(chapter: self!, shouldWrite: "You swallow a handfull of coins.")
//                if coins < 30 {
//                    self?.delegate?.chapter(chapter: self!, shouldWrite: "You are now dead due to greed.")
//                    abort()
//                }
//            }
//        ])
    }
    
}
