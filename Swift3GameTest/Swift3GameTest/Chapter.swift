//
//  AbstractWorld.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 25/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

protocol ChapterDelegate: class {
    func chapterShouldPrintLocation(chapter: Chapter)
    func chapter(chapter: Chapter, shouldWrite message: String)
    func chapterShouldEndGame(chapter: Chapter)
    func chapter(chapter: Chapter, shouldChangeTo newChapterId: String)
}

protocol Chapter: class, Rules {
    
    var player: Actor { get set }
    
    var items: [Item] { get }
    var actors: [Actor] { get }
    
    var directions: [Word] { get }
    var verbs: [Word] { get }
    var nouns: [Word] { get }
    var words: [Word] { get }
    
    var turnIncrement: ((_ count: Int) -> Void)? { get }
    var canPerformDirection: ((Direction) -> DirectionStatus)? { get }
    var enterChapter: (() -> ())? { get }
    
    var prompt: String { get }
    
    var delegate: ChapterDelegate? { get set }
    
    var activeMenu: Menu? { get set }
}

extension Chapter {
    
    func performDirection(direction: Direction) -> Bool {
        guard let exit = self.player.location?.exits.first(where: { $0.direction == direction }) else {
            return false
        }
        guard let canPerformDirection = self.canPerformDirection else {
            self.player.location = exit.location
            return true
        }
        let okToExit = canPerformDirection(direction)
        switch okToExit {
        case .ok:
            self.player.location = exit.location
            return true
        case .invalid:
            self.write("You cannot go that way.")
            return false
        case let .blocked(reason):
            if reason != nil {
                self.write(reason!)
            }
            return false
        }
    }

    func findKeyword(testWord: String) -> String? {
        if let index = self.words.index(where: { $0.isMatch(word: testWord) } ) {
            return self.words[index].keyword
        }
        return nil
    }
    
    func hasItem(itemName: String) -> Bool {
        return self.items.contains(where: { $0.keyword.lowercased() == itemName } )
    }
    
    func write(_ string: String) {
        self.delegate?.chapter(chapter: self, shouldWrite: string)
    }
    
    func say(_ dialogue: String, actor: Actor) {
        self.delegate?.chapter(chapter: self, shouldWrite: "\(actor.name): \"\(dialogue)\"")
    }
    
}
