//
//  Menu.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class Menu {
    let question: String?
    
    var items = [MenuItem]()
    
    var menu: String? {
        let options = self.items.filter( { $0.title != nil } )
        let optionStrings = options.map( { $0.showKey ? "\($0.key). \($0.title!)" : "\($0.title!)" } ).joined(separator: "\n")
        if !optionStrings.isEmpty {
            return self.question != nil ? "\(self.question!)\n\(optionStrings)" : "\(optionStrings)"
        }
        return self.question
    }
    
    var prompt: String {
        let options = self.items.map( { $0.key } )
        if options.count == 1 {
            return "\(options[0])? > "
        }
        let firstOptions = options.dropLast().joined(separator: ", ")
        return "\(firstOptions) or \(options.last!)? > "
    }
    
    init(question: String? = nil, items: [MenuItem]) {
        self.question = question
        self.items = items
    }
    
    func selectedMenuItem(words: [String]) -> MenuItem? {
        let keyword = words.first!
        return self.items.first(where: { $0.key.lowercased() == keyword } )
    }
    
    func processInput(menuItem: MenuItem) -> Menu? {
        menuItem.action()
        return menuItem.nextMenu
    }
}
