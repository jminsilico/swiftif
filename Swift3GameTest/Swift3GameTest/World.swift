//
//  World.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 25/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

protocol WorldDelegate: class {
    func worldShouldPrintLocation(world: World)
    func world(world: World, shouldWrite message: String)
    func worldShouldExit(world: World)
}

class World {
    
    weak var delegate: WorldDelegate?
    
    var chapters: [String: Chapter]
    var turnCount = 0
    
    init(chapters: [String: Chapter], initialChapterId: String) {
        self.chapters = chapters
        let initialChapter = self.chapters[initialChapterId]!
        self.setCurrentChapter(chapter: initialChapter)
    }
    
    func write(message: String) {
        guard let delegate = self.delegate else {
            print(message)
            return
        }
        delegate.world(world: self, shouldWrite: message)
    }
    
    func printLocation() {
        if let delegate = self.delegate {
            delegate.worldShouldPrintLocation(world: self)
            return
        }
        
        print("--- \(self.currentChapter.player.location!.name)")
        
        print(self.currentChapter.player.location!.description)
//        speak(string: self.currentChapter.currentLocation.description)
        
        let actors = self.currentChapter.actors.filter({ $0.location == self.currentChapter.player.location! })
        if actors.count > 0 {
            let actorNames = actors.map({ $0.name }).joined(separator: ", ")
            let additionalText = actors.count > 1 ? "are here" : "is here"
            print("\(actorNames) \(additionalText).")
        }
        
        if self.currentChapter.player.location!.exits.count > 0 {
            let exits = self.currentChapter.player.location!.exits.map( { "\($0.direction.name) to \($0.location.name)" } ).joined(separator: ", ")
            print("Available exits: \(exits)")
        }
    }
    
    func printPrompt() {
        let prompt = self.currentChapter.activeMenu?.prompt ?? self.currentChapter.prompt
        guard let delegate = self.delegate else {
            print("\(prompt)", terminator: "")
            return
        }
        delegate.world(world: self, shouldWrite: prompt)
    }
    
    var currentChapter: Chapter!
    func setCurrentChapter(chapter: Chapter) {
        self.currentChapter = chapter
        self.currentChapter.delegate = self
        if let enterChapter = self.currentChapter.enterChapter {
            enterChapter()
        } else {
            self.printLocation()
        }
    }
    
    func action(inputString: String) {
        self.process(input: inputString)
        
        self.turnCount = self.turnCount + 1
        self.currentChapter.turnIncrement?(self.turnCount)
        self.printPrompt()
    }
    
    func process(input: String) {
        guard input.characters.count > 0 else {
            return
        }
        
        let sanitisedInputString = input.sanitized
        var words = sanitisedInputString.lowercased().components(separatedBy: " ")
        
        var foundWords: String?
        var foundVerb = false
        var foundNouns = 0
        
        words = words.flatMap( { self.currentChapter.findKeyword(testWord: $0) } )
        for word in words {
            if let menu = self.currentChapter.activeMenu {
                // Process the menu
                if let selectedMenuItem = menu.selectedMenuItem(words: words) {
                    self.currentChapter.activeMenu = menu.processInput(menuItem: selectedMenuItem)
                    return
                }
                self.write(message: "I don't understand.")
                return
            }
            // Direction check.
            if !foundVerb && self.currentChapter.directions.contains(where: { $0.isMatch(word: word) } ) {
                if self.currentChapter.performDirection(direction: Direction(rawValue: word)!) == true {
                    self.printLocation()
                }
                return
            } else if !foundVerb && self.currentChapter.verbs.contains(where: { $0.isMatch(word: word) } ) {
                foundWords = word
                foundVerb = true
            } else if foundVerb == true
                && foundNouns < 2
                && (self.currentChapter.nouns.contains(where: { $0.isMatch(word: word) } ) == true || self.currentChapter.hasItem(itemName: word)) {
                foundWords = foundWords! + " " + word
                foundNouns += 1
            }
        }
        
        if !foundVerb {
            self.write(message: "I don't understand.")
            return
        }
        
        // Check for local rules in the current location.
        for rule in self.currentChapter.player.location!.rules {
            if rule.performCommand(testCommand: foundWords!) {
                return
            }
        }
        
        // Check for global rules.
        for rule in self.currentChapter.rules {
            if rule.performCommand(testCommand: foundWords!) {
                return
            }
        }
        
        self.write(message: "I don't understand.")
    }
    
    // MARK: - Process functions
    
//    func sanitiseInput(input: String) -> String {
//        
//    }
//    
//    func transformInputToWords(chapter: Chapter) -> ((String) -> [String]) {
//        return { input in
//            
//        }
//    }
//    
//    func processMenu(chapter: Chapter) -> (([String]) -> [String]) {
//        return { words in
//            
//        }
//    }
//    
//    func processDirection(chapter: Chapter) -> (([String]) -> [String]) {
//        return { words in
//            
//        }
//    }
//    
//    func transformWordsToVerbAndNouns(chapter: Chapter) -> (([String]) -> (verb: String, nouns: [String])) {
//        return { words in
//            
//        }
//    }
//    
//    func processChapterRules(chapter: Chapter) -> ((String, [String]) -> [String]) {
//        return { (verb, nouns) in
//            return []
//        }
//    }
//    
//    func processCurrentLocationRules(chapter: Chapter) -> ((String, [String]) -> [String]) {
//        return { (verb, nouns) in
//            return []
//        }
//    }
    
}

extension World: ChapterDelegate {
    
    func chapter(chapter: Chapter, shouldWrite message: String) {
        self.write(message: message)
    }
    
    func chapterShouldPrintLocation(chapter: Chapter) {
        self.printLocation()
    }
    
    func chapterShouldEndGame(chapter: Chapter) {
        self.write(message: "GAME OVER")
        exit(1)
    }
    
    func chapter(chapter: Chapter, shouldChangeTo newChapterId: String) {
        let newChapter = self.chapters[newChapterId]!
        self.setCurrentChapter(chapter: newChapter)
    }
    
}
