//
//  ChapterSecondFloor.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 24/10/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class ChapterSecondFloor: BaseChapter {

    init() {
        let player = Actor(key: "you", name: "You")
        super.init(player: player)
        
        self.addWord(type: .direction, keyword: "n", alternatives: ["north"])
        self.addWord(type: .direction, keyword: "s", alternatives: ["south"])
        self.addWord(type: .direction, keyword: "e", alternatives: ["east"])
        self.addWord(type: .direction, keyword: "w", alternatives: ["west"])
        self.addWord(type: .direction, keyword: "ne", alternatives: ["northeast"])
        self.addWord(type: .direction, keyword: "nw", alternatives: ["northwest"])
        self.addWord(type: .direction, keyword: "se", alternatives: ["southeast"])
        self.addWord(type: .direction, keyword: "sw", alternatives: ["southwest"])
        
        self.addWord(type: .noun, keyword: "1")
        self.addWord(type: .noun, keyword: "2")
        self.addWord(type: .noun, keyword: "3")
        
        self.addWord(type: .noun, keyword: "bendix")
        self.addWord(type: .noun, keyword: "mads")
        
        self.addWord(type: .noun, keyword: "banana")
        self.addWord(type: .noun, keyword: "peel")
        self.addWord(type: .noun, keyword: "b")
        self.addWord(type: .noun, keyword: "overalls")
        self.addWord(type: .noun, keyword: "fruit")
        self.addWord(type: .noun, keyword: "bowl")
        self.addWord(type: .noun, keyword: "fruitbowl")
        
        self.addWord(type: .verb, keyword: "eat")
        self.addWord(type: .verb, keyword: "drop")
        self.addWord(type: .verb, keyword: "give")
        self.addWord(type: .verb, keyword: "wear")
        
        let hallway = Location(name: "Hallway")
        self.player.location = hallway
        hallway.descriptionVariables = [
            (key: "hallway", value: { "A small hallway." })
        ]
        
        let garage = Location(name: "Garage")
        garage.descriptionVariables = [(key: "garage", value: { "The garage." })]
        
        let hallwayToGarageExit = Exit(direction: .north, location: garage)
        hallway.exits.append(hallwayToGarageExit)
        
        let garageToHallwayExit = Exit(direction: .south, location: hallway)
        garage.exits.append(garageToHallwayExit)
        
        let m2 = Location(name: "M2")
        let hallwayToM2Exit = Exit(direction: .southEast, location: m2)
        hallway.exits.append(hallwayToM2Exit)
        
        let m1 = Location(name: "M1")
        let hallwayToM1Exit = Exit(direction: .southWest, location: m1)
        hallway.exits.append(hallwayToM1Exit)
        
        let m1ToHallwayExit = Exit(direction: .northEast, location: hallway)
        m1.exits.append(m1ToHallwayExit)
        
        let insilico = Location(name: "Insilico")
        insilico.descriptionVariables = [(key: "insilico", value: { "Insilico office where all the magic happens." })]
        
        let garageToInsilicoExit = Exit(direction: .west, location: insilico)
        garage.exits.append(garageToInsilicoExit)
        
        let insilicoToGarageExit = Exit(direction: .east, location: garage)
        insilico.exits.append(insilicoToGarageExit)
        
        let m10 = Location(name: "M10")
        let insilicoToM10Exit = Exit(direction: .west, location: m10)
        insilico.exits.append(insilicoToM10Exit)
        
        let m10ToInsilicoExit = Exit(direction: .east, location: insilico)
        m10.exits.append(m10ToInsilicoExit)
        
        let overalls = WearableItem(keyword: "overalls", shortDescription: "Overalls.", longDescription: "Cleaners overalls.")
        self.items.append(overalls)
        
        let cleaningCupboard = Location(name: "Cleaning Cupboard")
        cleaningCupboard.descriptionVariables = [(key: "cleaningCupboard", value: {
            "Small cupboard filled with crap. There is paper towels, first aid kit, Tom Cruise, cleaning utensils, and a sink."
        }), (key: "overalls", value: {
            if overalls.location == cleaningCupboard {
                return "There are overalls hanging over a broom-handle."
            }
            return nil
        })]
        overalls.location = cleaningCupboard

        let insilicoToUtilityCupboardExit = Exit(direction: .northWest, location: cleaningCupboard)
        insilico.exits.append(insilicoToUtilityCupboardExit)
        
        let utilityCupboardToInsilicoExit = Exit(direction: .southEast, location: insilico)
        cleaningCupboard.exits.append(utilityCupboardToInsilicoExit)
        
        let insilicoWardrobe = Location(name: "Insilico wardrobe")
        insilicoWardrobe.descriptionVariables = [(key: "insilicoWardrobe", value: { "There is a fridge and some coats hanging. It kind of stinks in here. Two doors leading to toilets that are of course occupied. You can hear someone crying from inside one of them." } )]
        
        let insilicoToWardrobeExit = Exit(direction: .north, location: insilicoWardrobe)
        insilico.exits.append(insilicoToWardrobeExit)
        
        let wardrobeToInsilicoExit = Exit(direction: .south, location: insilico)
        insilicoWardrobe.exits.append(wardrobeToInsilicoExit)
        
        let reception = Location(name: "Reception")
        reception.descriptionVariables = [(key: "reception", value: { "You are at the reception area." })]
        
        let garageToReceptionExit = Exit(direction: .east, location: reception)
        garage.exits.append(garageToReceptionExit)
        
        let receptionToGarageExit = Exit(direction: .west, location: garage)
        reception.exits.append(receptionToGarageExit)
        
        let hallwayToReceptionExit = Exit(direction: .northEast, location: reception)
        hallway.exits.append(hallwayToReceptionExit)
        
        let receptionToHallwayExit = Exit(direction: .southWest, location: hallway)
        reception.exits.append(receptionToHallwayExit)
        
        let in2Media = Location(name: "In2Media")
        in2Media.descriptionVariables = [(key: "in2Media", value: { "Lot's of beatiful design tables and design designers. They seem busy. Or not." })]
        
        let receptionToIn2MediaExit = Exit(direction: .north, location: in2Media)
        reception.exits.append(receptionToIn2MediaExit)
        
        let in2MediaToReceptionExit = Exit(direction: .south, location: reception)
        in2Media.exits.append(in2MediaToReceptionExit)
        
        let coffeeCouchArea = Location(name: "Coffee and couch area")
        coffeeCouchArea.descriptionVariables = [(key: "coffeeCouchArea", value: { "Coffee machine which is occupied. Fridge full with soft drinks and beer. A fruit bowl filled with various fruit.  Comfy couch in the corner. And a huuugeee loud AC unit encapsuled in yellow plastic." })]
        
        let in2MediaToCoffeeCouchAreaExit = Exit(direction: .north, location: coffeeCouchArea)
        in2Media.exits.append(in2MediaToCoffeeCouchAreaExit)
        
        let coffeeCouchAreaToIn2MediaExit = Exit(direction: .south, location: in2Media)
        coffeeCouchArea.exits.append(coffeeCouchAreaToIn2MediaExit)
        
        let baby = Location(name: "BABY")
        let floor = Location(name: "floor")
        
        let receptionToBabyExit = Exit(direction: .east, location: baby)
        reception.exits.append(receptionToBabyExit)
        
        let babyToReceptionExit = Exit(direction: .west, location: reception)
        baby.exits.append(babyToReceptionExit)
        
        let babyHallway = Location(name: "BABY hallway")
        babyHallway.descriptionVariables = [(key: "babyHallway", value: { "Small hallway." })]
        
        let babyToBabyHallwayExit = Exit(direction: .south, location: babyHallway)
        baby.exits.append(babyToBabyHallwayExit)
        
        let babyHallwayToBabyExit = Exit(direction: .north, location: baby)
        babyHallway.exits.append(babyHallwayToBabyExit)
        
        let m4 = Location(name: "M4")
        let babyHallwayToM4Exit = Exit(direction: .south, location: m4)
        babyHallway.exits.append(babyHallwayToM4Exit)
        
        let m4ToBabyHallwayExit = Exit(direction: .north, location: babyHallway)
        m4.exits.append(m4ToBabyHallwayExit)
        
        let in2Toiletts = Location(name: "In2 Toilets")
        let babyHallwayToIn2ToilletsExit = Exit(direction: .southEast, location: in2Toiletts)
        babyHallway.exits.append(babyHallwayToIn2ToilletsExit)
        
        let in2ToilletsToBabyHallwayExit = Exit(direction: .northWest, location: babyHallway)
        in2Toiletts.exits.append(in2ToilletsToBabyHallwayExit)
        
        let pantry = Location(name: "Cupboard")
        pantry.descriptionVariables = [(key: "pantry", value: { "Ah ha! So this is where Louise is hiding the crisps." })]
        let pantryToBabyHallwayExit = Exit(direction: .west, location: babyHallway)
        pantry.exits.append(pantryToBabyHallwayExit)
        
        let babyHallwayToPantryExit = Exit(direction: .east, location: pantry)
        babyHallway.exits.append(babyHallwayToPantryExit)
        
        //FIXME: mads should be on e hoverboard with a coffee.
        let mads = Actor(key: "mads", name: "Mads")
        mads.location = baby
        self.actors.append(mads)
        
        let banana = Item(keyword: "banana", shortDescription: "Banana", longDescription: "An organic, EU compliant, slightly curved yellow fruit.", location: coffeeCouchArea)
        self.items.append(banana)
        
        coffeeCouchArea.addRule(commands: ["look bowl", "look fruit", "look fruit bowl", "look fruitbowl"], conditions: [], actions: [ { command in
            guard banana.location == coffeeCouchArea else {
                self.write("The fruit bowl has a couple of apples and some mandarins.")
                return
            }
            self.write("The fruit bowl has a banana, a couple of apples, and some grapes.")
        }])
        
        let peel = Item(keyword: "peel", shortDescription: "Banana peel.", longDescription: "Very slippery banana peel.")
        self.items.append(peel)
        
        let goldB = Item(keyword: "b", shortDescription: "Gold B.", longDescription: "Huge B made of gold. Someone is overcompensating for something.", location: baby)
        self.items.append(goldB)
        
        let dirtyB = Item(keyword: "b", shortDescription: "Dirty gold B.", longDescription: "Huge gold B decorated with coffee")
        
        baby.descriptionVariables = [(key: "baby", value: {
            "Everything seems a little bit more fancy here."
        }), (key: "gold B", value: {
            if goldB.location == baby {
                return "Especially an enormous, shiny golden 'B'. Someone is clearly overcompensating for something."
            }
            return nil
        }), (key: "dirty B", value: {
            if dirtyB.location == baby {
                return "Especially an enormous golden 'B'. It was shiny once, now it's decorated with coffee. Is this some new trend i am not aware of?"
            }
            return nil
        }), (key: "no B", value: {
            if dirtyB.location == self.player.inventory {
                return "There is a shadow on the wall from an enormous B."
            }
            return nil
        })]
        
        self.addRule(commands: ["look banana peel", "look banana", "look peel"], conditions: [{ self.player.inventory == peel.location }], actions: [
            { command in
                self.write(peel.longDescription)
            }
        ])
        
        self.addRule(commands: ["eat banana"], conditions: [{ self.player.inventory == banana.location }], actions: [
            { command in
                banana.location = nil
                peel.location = self.player.inventory
                self.write("You eat the banana. Now you need to do something with this banana peel.")
            }
        ])
        
        self.addRule(commands: ["drop banana peel", "drop peel", "drop banana"], conditions: [{ peel.location == self.player.inventory  }], actions: [
            { command in
                guard self.player.location == baby else {
                    self.write(["There must be a better place to drop this.", "I can't drop this here, someone might slip on it."].sample!)
                    return
                }
                peel.location = baby
                self.write("You drop the banana peel on the floor. I hope that nobody slips on it.")
            }
        ])
        
        self.addRule(commands: ["wear overalls"], conditions: [{ self.player.inventory == overalls.location }], actions: [{ command in
            guard !overalls.worn else {
                self.write("You are already wearing them")
                return
            }
            overalls.worn = true
            self.write("You put on the overalls. Now you look like a cleaner.")
        }])
        
        let bendix = Actor(key: "bendix", name: "The ghost of Bendix")
        bendix.location = hallway
        self.actors.append(bendix)
        
        baby.addRule(commands: ["take b"], conditions: [], actions: [
            { command in
                guard dirtyB.location == self.player.inventory else {
                    return
                }
                self.write("You already have the gold B decorated with coffee in your inventory.")
            },
            { command in
                guard dirtyB.location == baby && overalls.worn else {
                    return
                }
                dirtyB.location = self.player.inventory
                self.write("You take the gold B decorated with coffee.")
            },
            { command in
                guard dirtyB.location == baby && !overalls.worn else {
                    return
                }
                self.write("Mads is laying on the floor, screaming")
                self.say("A-A-A. Don't you dare touch that B.", actor: mads)
            },
            { command in
                guard peel.location == baby && goldB.location == baby else {
                    return
                }
                self.write("A furious Mads scretches into the room on the hoverboard, slips on the banana peel and slides straight into the wall. There is coffee all over. His precious B is really dirty now.")
                mads.location = floor
                goldB.location = nil
                dirtyB.location = baby
                self.items.append(dirtyB)
            },
            { command in
                guard goldB.location == baby && peel.location != baby else {
                    return
                }
                self.write("Mads appears in between you and the B.")
                self.say("A-A-A. Don't you dare touch that B.", actor: mads)
            }
        ])
        
        self.addRule(commands: ["give b"], conditions: [{ self.player.inventory == goldB.location }], actions: [
            { command in
                self.write("To whom? Who would want a giant gold B?")
            }
        ])

        self.addRule(commands: ["give b bendix"], conditions: [{ self.player.inventory == dirtyB.location }], actions: [
            { commaind in
                guard bendix.location == self.player.location else {
                    self.write("Bendix is not here.")
                    return
                }
                dirtyB.location = nil
                self.write("You give the B to Bendix, so you get his precious time traveling  Watch™.")
            }
        ])
        
        var watchCost = 2000
        
        let ghostBendixWatch3MenuItemOne = MenuItem(key: "1", title: "I'll manage without it.".quote) {
            self.say("You'll be back.", actor: bendix)
        }
        let ghostBendixWatch3MenuItemTwo = MenuItem(key: "2", title: "I don't have that kind of cash.".quote) {
            self.say("You'll be back.", actor: bendix)
        }
        let ghostBendixWatch3Menu = Menu(items: [ghostBendixWatch3MenuItemOne, ghostBendixWatch3MenuItemTwo])
        
        let ghostBendixWatch2MenuItemOne = MenuItem(key: "1", title: "@{watchCost} dkk?! How am I supposed to help you without it?".quote, nextMenu: ghostBendixWatch3Menu, variables: ["watchCost": { String(watchCost) }]) {
            self.say("\(watchCost) dkk?! But how am I supposed to help you without it?", actor: self.player)
            watchCost += 500
            self.say("You can't, and it's going to cost you \(watchCost) dkk.", actor: bendix)
        }
        let ghostBendixWatch2MenuItemTwo = MenuItem(key: "2", title: "Come on, it can't be worth more than @{watchCost} dkk.".quote, variables: ["watchCost": { String(watchCost - 150) }]) {
            self.say("Fine, but I'll only give you \(watchCost - 150) dkk.", actor: self.player)
            watchCost += 500
            self.say("Nope, the price is fixed, it's going to cost you \(watchCost) dkk.", actor: bendix)
        }
        let ghostBendixWatch2MenuItemThree = MenuItem(key: "3", title: "\"MobilePay?\"", nextMenu: ghostBendixWatch3Menu) {
            self.say("I'll MobilePay you.", actor: self.player)
            self.say("Nice try. Cold hard cash! Or gold, I'll take gold.", actor: bendix)
        }
        let ghostBendixWatch2Menu = Menu(items: [ghostBendixWatch2MenuItemOne, ghostBendixWatch2MenuItemTwo, ghostBendixWatch2MenuItemThree])
        ghostBendixWatch2MenuItemTwo.nextMenu = ghostBendixWatch2Menu
        
        let ghostBendixWatchMenuItemOne = MenuItem(key: "1", title: "Take watch", nextMenu: ghostBendixWatch2Menu) {
            self.say("Gee, thanks, I've always wanted one of these.", actor: self.player)
            self.say("Not so fast, it's going to cost you \(watchCost) dkk.", actor: bendix)
        }
        let ghostBendixWatchMenu = Menu(items: [ghostBendixWatchMenuItemOne])
        
        let ghostBendixIntroMenuAction = {
            self.say("AAAAARGH", actor: self.player)
            self.write("You pass out at the sight of a ghost. When you awake the ghost of Bendix it's still there.")
            self.say("I'm not going anywhere! In order to fix this mess you must use this time travelling  Watch™.", actor: bendix)
        }
        let ghostBendixIntroMenuItemOne = MenuItem(key: "1", title: "Okay, what do I need to do?".quote, nextMenu: ghostBendixWatchMenu, action: ghostBendixIntroMenuAction)
        let ghostBendixIntroMenuItemTwo = MenuItem(key: "2", title: "How do I know you are who you say you are?".quote, nextMenu: ghostBendixWatchMenu, action: ghostBendixIntroMenuAction)
        let ghostBendixIntroMenuItemThree = MenuItem(key: "3", title: "I don't believe in ghosts.".quote, nextMenu: ghostBendixWatchMenu, action: ghostBendixIntroMenuAction)
        let ghostBendixIntroMenu = Menu(question: "What do you have to say for yourself?", items: [ghostBendixIntroMenuItemOne, ghostBendixIntroMenuItemTwo, ghostBendixIntroMenuItemThree])
        self.activeMenu = ghostBendixIntroMenu
        
        bendix.talkMenu = ghostBendixWatch2Menu
        
        let madsRandomMovement  = {
            let validLocations = [baby, reception, in2Media, insilico, coffeeCouchArea, garage]
            if let exit = mads.location?.exits.filter( { validLocations.contains($0.location) } ).sample {
                if mads.location == self.player.location {
                    print("Mads has gone to \(exit.location.name)")
                }
                mads.location = exit.location
                if mads.location == self.player.location {
                    print("Mads is now here.")
                    self.say("Blah blah blah blah blah blah!", actor: mads)
                }
            }
        }
        
        self.turnIncrement = { (turn) in
            madsRandomMovement()
        }
        
        self.enterChapter = {
            self.delegate?.chapterShouldPrintLocation(chapter: self)
            self.write("A ghostly figure appears in front of you.")
            self.say("Wooooooooooooooo! I am the ghost of Bendix! Things will go horribly wrong since I am not here anymore. You are to blame!! You must fix this!", actor: bendix)
            self.activeMenu = ghostBendixIntroMenu
        }
    }
    
}
