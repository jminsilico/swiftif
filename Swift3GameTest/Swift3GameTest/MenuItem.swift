//
//  MenuItem.swift
//  Swift3GameTest
//
//  Created by Jonathan McAllister on 01/11/2016.
//  Copyright © 2016 InSilico. All rights reserved.
//

import Foundation

class MenuItem {
    let key: String
    private let _title: String?
    var title: String? {
        guard var title = self._title else {
            return nil
        }
        if self.variables != nil {
            for (key, value) in self.variables! {
                title = title.replacingOccurrences(of: "@{\(key)}", with: value())
            }
        }
        return title
    }
    let showKey: Bool
    let action: () -> ()
    var nextMenu: Menu?
    let variables: [String: () -> String]?
    
    init(key: String, title: String? = nil, showKey: Bool = true, nextMenu: Menu? = nil, variables: [String: () -> String]? = nil, action: @escaping () -> ()) {
        self.key = key
        self._title = title
        self.showKey = showKey
        self.nextMenu = nextMenu
        self.action = action
        self.variables = variables
    }
}
