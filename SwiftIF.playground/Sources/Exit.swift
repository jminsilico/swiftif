//
//  Exit.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 25/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import UIKit

enum Direction: String {
    case North = "N"
    case East  = "E"
    case South = "S"
    case West  = "W"
    
    var name: String {
        switch self {
        case .North: return "North"
        case .East:  return "East"
        case .South: return "South"
        case .West:  return "West"
        }
    }
}

struct Exit: Equatable {
    let direction: Direction
    let location: Location
}

func == (lhs: Exit, rhs: Exit) -> Bool {
    return (lhs.direction == rhs.direction) && (lhs.location == rhs.location)
}

struct Location: Equatable {
    var title: String
    var description: String
    var exits: [Exit]
    var rules: [Rule]
}

func == (lhs: Location, rhs: Location) -> Bool {
    return (lhs.title == rhs.title)
        && (lhs.description == rhs.description)
        && (lhs.exits == rhs.exits)
}

struct Rule: Equatable {
    let commands: [String]
    let conditions: [() -> Bool]
    let actions: [(command: String) -> ()]
    
    func performCommand(testCommand: String) -> Bool {
        for command in self.commands {
            if self.compareCommand(testCommand, rule: command) && self.testConditions() {
                self.performActions(command)
                return true
            }
        }
        return false
    }
    
    func compareCommand(input: String, rule: String) -> Bool {
        if rule.lowercaseString.rangeOfString("*") == nil {
            return input == rule
        }
        let inputTokens = input.componentsSeparatedByString(" ")
        let ruleTokens = rule.componentsSeparatedByString(" ")
        for var i = 0; i < inputTokens.count; ++i {
            if (i >= ruleTokens.count || ruleTokens.last! != "*") && ruleTokens[i] != "*" && inputTokens[i] != ruleTokens[i] {
                return false
            }
        }
        return true
    }
    
    func testConditions() -> Bool {
        for condition in self.conditions {
            if !condition() {
                return false
            }
        }
        return true
    }
    
    func performActions(command: String) {
        self.actions.forEach( { $0(command: command) } )
    }
}

func == (lhs: Rule, rhs: Rule) -> Bool {
    return (lhs.commands == rhs.commands)
}

struct Item {
    let keyword: String
    let shortDescription: String
    private let _longDescription: String
    var longDescription: String {
        var description = self._longDescription
        for (key, value) in self.variables {
            description = description.stringByReplacingOccurrencesOfString("@{\(key)}", withString: value())
        }
        return description
    }
    let location: Location
    let variables: [String: () -> String]
}

enum WordType {
    case Direction
    case Verb
    case Noun
}

struct Word {
    let type: WordType
    let keyword: String
    let alternatives: [String]?
    
    func isMatch(word: String) -> Bool {
        if word == self.keyword || self.alternatives?.contains(word) == true {
            return true
        }
        return false
    }
}

extension RangeReplaceableCollectionType where Generator.Element : Equatable {
    mutating func removeObject(object : Generator.Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
}
