//
//  AbstractWorld.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 25/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

struct AbstractChapter {
    var currentLocation: Location
    var intentory: Location
    
    private var _items = [Item]()
    
    func hasItem(itemName: String) -> Bool {
        return self._items.contains( { $0.keyword == itemName } )
    }
    
    var directions: [Word] {
        return self._words.filter( { $0.type == WordType.Direction } )
    }
    var verbs: [Word] {
        return self._words.filter( { $0.type == WordType.Verb } )
    }
    var nouns: [Word] {
        return self._words.filter( { $0.type == WordType.Noun } )
    }
    private var _words = [Word]()
    
    var rules = [Rule]()
    
    mutating func performDirection(direction: Direction) -> Bool {
        if let index = self.currentLocation.exits.indexOf( { $0.direction == direction } ) {
            let exit = self.currentLocation.exits[index]
            self.currentLocation = exit.location
            return true
        }
        return false
    }
    
    func findKeyword(testWord: String) -> String? {
        if let index = self._words.indexOf( { $0.isMatch(testWord) } ) {
            return self._words[index].keyword
        }
        return nil
    }
    
}

struct World {
    
    func write(message: String) {
        print(message)
    }
    
    func printLocation() {
        print("")
        print(currentChapter!.currentLocation.title)
        print("")
        print(currentChapter!.currentLocation.description)
        print("")
        print("Available exits: \(currentChapter!.currentLocation.exits)")
        print("")
    }
    
    var currentChapter: AbstractChapter?
    
    mutating func action(inputString: String) {
        if inputString.characters.count == 0 {
            return
        }
        
        var words = inputString.componentsSeparatedByString(" ")
        var foundWords: String?
        var foundVerb = false
        var foundNouns = 0
        
        words = words.flatMap( { self.currentChapter!.findKeyword($0) } )
        for word in words {
            // Direction check.
            if !foundVerb && self.currentChapter!.directions.contains( { $0.isMatch(word) } ) {
                if self.currentChapter!.performDirection(Direction(rawValue: word)!) == true {
                    self.printLocation()
                } else {
                    self.write("You cannot go that way.")
                }
                return
            } else if !foundVerb && self.currentChapter!.verbs.contains( { $0.isMatch(word) } ) {
                foundWords = word
                foundVerb = true
            } else if foundVerb && foundNouns < 2 && self.currentChapter?.nouns.contains( { $0.isMatch(word) } ) == true || self.currentChapter!.hasItem(word) {
                foundWords = foundWords! + word
                ++foundNouns
            }
        }
        
        if !foundVerb {
            self.write("I don't understand.")
            return
        }
        
        // Check for global rules.
        for rule in self.currentChapter!.rules {
            if rule.performCommand(foundWords!) {
                return
            }
        }
        
        // Check for local rules in the current location.
        for rule in self.currentChapter!.currentLocation.rules {
            if rule.performCommand(foundWords!) {
                return
            }
        }
        
        self.write("I don't understand.")
    }
    
}
