//
//  Option.swift
//  SwiftIF
//
//  Created by Jonathan McAllister on 26/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

struct Option {
    let names: [String]
    let argumentCount: Int
    let command: (option: String) -> ()
    var description: String {
        return "[\(self.names.map( { "-\($0)" } ).joinWithSeparator("|"))]"
    }
}

func processOptions(var options: [Option], arguments: [String]) {
    options.insert(Option(names: ["h", "help"], argumentCount: 0, command: { (option) -> () in
        let commandName = NSURL(string: Process.arguments[0])!.lastPathComponent!
        let optionsString = options.reduce("", combine: { $0 + " " + $1.description })
        print("usage: \(commandName)\(optionsString)")
        abort()
    }), atIndex: 0)
    argumentIterator: for var i = 0; i < arguments.count; ++i {
        let argument = arguments[i].stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "-"))
        for option in options {
            if option.names.contains(argument) && i + option.argumentCount < arguments.count {
                option.command(option: arguments[i + option.argumentCount])
                continue argumentIterator
            }
        }
    }
}

//if let index = Process.arguments.indexOf( { $0 == "-prompt" || $0 == "-p" } ) where index + 1 < Process.arguments.count {
//    promptString = Process.arguments[index + 1] + " "
//}
