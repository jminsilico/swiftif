//
//  main.swift
//  SwiftIFCL
//
//  Created by Jonathan McAllister on 26/11/15.
//  Copyright © 2015 Rocket440. All rights reserved.
//

import Foundation

let chapter0 = Chapter0()
var world = World(chapter: chapter0)
world.printLocation()

var promptString = "What now? >"

let promptOption = Option(names: ["p", "prompt"], argumentCount: 1, command: { (option) in
    promptString = option
})
processOptions([promptOption], arguments: Process.arguments)

func prompt() {
    print(promptString, terminator: "")
}

func waitForInput() {
    waitingForInput: while true {
        if let inputString = readLine() {
            world.action(inputString)
            prompt()
        }
    }
}

prompt()
waitForInput()
